(function() {
	angular.module('kickstart').controller('sidenavCtrl', sidenavCtrl);

	function sidenavCtrl(localStorageService, $state, $rootScope, userService, $mdSidenav) {
		var sidenav = this;
		sidenav.sideAvatar = userService.avatar;
		sidenav.sideName = userService.name;
		// sidenav.sideUsername = userService.username;
		sidenav.userId = userService.userId;
		$rootScope.$on('userAuthenticated', function() {
			sidenav.sideAvatar = userService.avatar;
			sidenav.sideName = userService.name;
			// sidenav.sideUsername = userService.username;
			sidenav.userId = userService.userId;
		})
		sidenav.goToVacancies=function(){
			$mdSidenav('menu').close();
			$state.go('main.vacancies');
		}
		sidenav.goToJobseekers=function(){
			$mdSidenav('menu').close();
			$state.go('main.jobseekers');
		}
		sidenav.goToLeaders=function(){
			$mdSidenav('menu').close();
			$state.go('main.leaders');
		}
		sidenav.goToInterviews=function(){
			$mdSidenav('menu').close();
			$state.go('main.interviews');
		}
		sidenav.logout = function() {
			$mdSidenav('menu').close();
			userService.removeParameters();
		}
	}
})();