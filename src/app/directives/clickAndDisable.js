(function() {
    angular.module('kickstart').directive('clickAndDisable', function($timeout) {
        return {
            scope: {
                onClick: '&'
            },
            link: function(scope, iElement, iAttrs) {
                var callbacks = {
                    formSubmitted: formSubmitted
                };

                //input click action
                angular.element(iElement).bind('click', function() {
                    formSubmitting();
                    $timeout(function() {

                        if (scope.onClick &&  typeof scope.onClick == 'function') {
                            scope.onClick({callbacks:callbacks});
                        } else {
                            formSubmitted();
                        }
                    }, 100);
                });


                //form submitted callback
                function formSubmitting() {
                    iElement.attr("disabled", "disabled");
                }

                //form submitted callback
                function formSubmitted() {
                    iElement.removeAttr("disabled");
                }
            }
        };
    });
})();
