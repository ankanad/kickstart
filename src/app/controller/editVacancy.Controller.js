(function() {
    angular.module('kickstart').controller('editVacancyCtrl', editVacancyCtrl);

    function editVacancyCtrl(appService, toastService, $scope, $state, $stateParams,$timeout) {
        var editVacancy = this;
        var vacancy = {
            id: $stateParams.vacancyId
        };
        appService.getTrades().then(function(response){
            editVacancy.allDropdowns=response.data;
        })
        appService.getVacancies(vacancy).then(function(result) {
            editVacancy.data = result.data[0];
            // editVacancy.data.educationLevel=editVacancy.data.educationLevel.toString();
            editVacancy.data.lat = result.data[0].location.lat;
            editVacancy.data.long = result.data[0].location.long;
            editVacancy.data.files = [];
            editVacancy.data.interviewDates=[];
            editVacancy.data.interview_dates.forEach(function(value){
                editVacancy.data.interviewDates.push({date:new Date(parseInt(value))});
            })
        });
        editVacancy.addDate = function() {
            editVacancy.data.interviewDates.push({ date:''});
        }
        editVacancy.removeDate = function(index) {
            editVacancy.data.interviewDates.splice(index, 1);
        }

        editVacancy.getFile = function(files, $errors) {
            if (angular.isDefined($errors) && ($errors.filesize.length || $errors.incorrectFile.length)) {
                var imageError = '';
                if ($errors.filesize.length) {
                    imageError += 'File size exceeds 5 mb for ' + $errors.filesize.join(', ');
                }
                if ($errors.incorrectFile.length) {
                    imageError += '\n Incorrect file type for ' + $errors.incorrectFile.join(', ');
                }
                toastService.showError(imageError);
            } else {
                editVacancy.data.files = files;
            }
        }
        editVacancy.edit = function(callbacks) {
            appService.editVacancy(editVacancy.data, vacancy.id).then(function(result) {
                $timeout(function() {
                $state.go('main.vacancies');
                toastService.show('Vacancy created');
                }, 1000);
            }).finally(function(){
                callbacks.formSubmitted();
            });
        }
        editVacancy.goToVacancies = function() {
            $state.go('main.vacancies');

        }
    }
})();
