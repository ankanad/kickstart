(function() {
    'use strict';
    angular.module('kickstart').config(routerConfig);
    /** @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider, $locationProvider, RestangularProvider) {
        $locationProvider.hashPrefix('!');
        $urlRouterProvider.otherwise('/login');
        $stateProvider.state('login', {
            url: '/login',
            templateUrl: 'app/views/login.html',
            onEnter: function(globalService, $rootScope) {
                if (globalService.getIsAuthorised()) {
                    $rootScope.$broadcast('userAuthenticated');
                }
            }
        }).state('main', {
            abstract: true,
            url: '/',
            templateUrl: 'app/views/main.html',
            onEnter: function(globalService, $rootScope) {
                if (!globalService.getIsAuthorised()) {
                    $rootScope.$broadcast('userUnauthenticated');
                }
            }
        }).state('main.dashboard', {
            url: 'dashboard',
            templateUrl: 'app/views/dashboard.html'
        }).state('main.vacancies', {
            url: 'vacancies',
            templateUrl: 'app/views/vacancies.html',
            controller: 'vacanciesCtrl',
            controllerAs: 'vacancies'
        }).state('main.createVacancy', {
            url: 'createVacancy',
            templateUrl: 'app/views/createVacancy.html',
            controller: 'createVacancyCtrl',
            controllerAs: 'createVacancy'
        }).state('main.editVacancy', {
            url: 'editVacancy/:vacancyId',
            templateUrl: 'app/views/editVacancy.html',
            controller: 'editVacancyCtrl',
            controllerAs: 'editVacancy'
        }).state('main.jobseekers', {
            url: 'jobseekers',
            templateUrl: 'app/views/jobseekers.html',
            controller: 'jobseekersCtrl',
            controllerAs: 'jobseekers'
        }).state('main.createJobseeker', {
            url: 'createJobseeker',
            templateUrl: 'app/views/createJobseeker.html',
            controller: 'createJobseekerCtrl',
            controllerAs: 'createJobseeker'
        }).state('main.editJobseeker', {
            url: 'editJobseeker/:jobseekerId',
            templateUrl: 'app/views/editJobseeker.html',
            controller: 'editJobseekerCtrl',
            controllerAs: 'editJobseeker'
        }).state('main.leaders', {
            url: 'leaders',
            templateUrl: 'app/views/leaders.html',
            controller: 'leadersCtrl',
            controllerAs: 'leaders'
        }).state('main.createLeader', {
            url: 'createLeader',
            templateUrl: 'app/views/createLeader.html',
            controller: 'createLeaderCtrl',
            controllerAs: 'createLeader'
        }).state('main.editLeader', {
            url: 'editLeader/:leaderId',
            templateUrl: 'app/views/editLeader.html',
            controller: 'editLeaderCtrl',
            controllerAs: 'editLeader'
        }).state('main.interviews', {
            url: 'interviews',
            templateUrl: 'app/views/interviews.html',
            controller: 'interviewsCtrl',
            controllerAs: 'interviews'
        }).state('main.createInterview', {
            url: 'createInterview',
            templateUrl: 'app/views/createInterview.html',
            controller: 'createInterviewCtrl',
            controllerAs: 'createInterview'
        }).state('main.editInterview', {
            url: 'editInterview/:interviewId',
            templateUrl: 'app/views/editInterview.html',
            controller: 'editInterviewCtrl',
            controllerAs: 'editInterview'
        });
    }
})()
