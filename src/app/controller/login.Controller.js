(function() {
	angular.module('kickstart').controller('loginCtrl', loginCtrl);

	function loginCtrl(publicService,userService, $state, toastService) {
		var user = this;
		user.userLogin = function() {
			publicService.login(user).then(function(response) {
				userService.saveLoggedInUser(response);
				toastService.show('Logged in successfully');
				$state.go('main.dashboard');
			}, function(error) {
				toastService.showError(error.data.errors[0]);
			});
		};
		user.userSignup = function() {
			publicService.signup(user).then(function(response) {
				// userService.saveLoggedInUser(response);
				toastService.show('Signed up successfully');
				// $state.go('main.dashboard');
			});
		};
	}
})();