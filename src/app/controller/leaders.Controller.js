(function() {
    angular.module('kickstart').controller('leadersCtrl', leadersCtrl);

    function leadersCtrl(appService, toastService, $scope, $state, $mdDialog) {
        var leaders = this;
        leaders.query = {
            limit: 5,
            page: 1
        };
        leaders.getLeaders = function(page, limit) {
            leaders.query.limit = limit;
            leaders.query.page = page;
            appService.getLeaders(leaders.query).then(function(response) {
                leaders.leaders = response;
            });
        }
        leaders.showDetails = function(leader) {
            $mdDialog.show({
                locals: { leader: leader },
                clickOutsideToClose: true,
                templateUrl: 'app/views/leader.html',
                controller: function($scope, leader) {
                    $scope.leader = leader;
                }
            });
        }

        leaders.getLeaders(leaders.query.page, leaders.query.limit);
        leaders.goToCreateLeader = function() {
            $state.go('main.createLeader');
        }
        leaders.goToEditLeader = function(event, leaderId) {
            event.stopPropagation();
            $state.go('main.editLeader', { leaderId: leaderId });
        }
        leaders.deleteLeader = function(event, leaderId, index) {
            event.stopPropagation();
            appService.deleteLeader(leaderId).then(function(response) {
                toastService.show('Leader deleted successfully');
                leaders.getLeaders(leaders.query.page, leaders.query.limit);
            });
        }

    }
})();
