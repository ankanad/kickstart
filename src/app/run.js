(function() {
    'use strict';
    angular.module('kickstart').run(runBlock);
    /** @ngInject */
    function runBlock(Restangular, globalService, userService, $rootScope, $state, toastr) {
        Restangular.setBaseUrl(globalService.getApiBaseUrl());
        Restangular.setErrorInterceptor(function(response) {
            if (response.status == 401) {
                userService.removeParameters();
                return false;
            }
            if (response.data && response.data.errors && response.data.errors.length) {
                toastr.error(response.data.errors[0]);
                return true;
            }
        });
        if (globalService.getIsAuthorised()) {
            userService.setParameters();
        }
        $rootScope.minDate = new Date(new Date().setYear(new Date().getFullYear() - 1));
        $rootScope.maxDate = new Date(new Date().setYear(new Date().getFullYear() + 1));
    }
})();
