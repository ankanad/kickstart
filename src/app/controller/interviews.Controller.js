(function() {
    angular.module('kickstart').controller('interviewsCtrl', interviewsCtrl);

    function interviewsCtrl(appService, toastService, $scope, $state, $mdDialog) {
        var interviews = this;
        interviews.query = {
            limit: 5,
            page: 1
        };
        interviews.getInterviews = function(page, limit) {
            interviews.query.limit = limit;
            interviews.query.page = page;
            appService.getInterviews(interviews.query).then(function(response) {
                interviews.interviews = response;
            });
        }


        interviews.getInterviews(interviews.query.page, interviews.query.limit);
        interviews.goToCreateInterview = function() {
            $state.go('main.createInterview');
        }
        interviews.goToEditInterview = function(event, interviewId) {
            event.stopPropagation();
            $state.go('main.editInterview', { interviewId: interviewId });
        }
        interviews.deleteInterview = function(event, interviewId, index) {
            event.stopPropagation();
            appService.deleteInterview(interviewId).then(function(response) {
                toastService.show('Interview deleted successfully');
                interviews.getInterviews(interviews.query.page, interviews.query.limit);
            });
        }

    }
})();
