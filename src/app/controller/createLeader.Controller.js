(function() {
    angular.module('kickstart').controller('createLeaderCtrl', createLeaderCtrl);

    function createLeaderCtrl(appService, toastService, $scope, $state, $timeout) {
        var createLeader = this;
        createLeader.data = {};

        createLeader.create = function(callbacks) {
            appService.createLeader(createLeader.data).then(function(result) {
                $timeout(function() {
                    $state.go('main.leaders');
                    toastService.show('Leader created');
                }, 1000);

            }).finally(function() {
                callbacks.formSubmitted();
            })
        }
        createLeader.goToLeaders = function() {
            $state.go('main.leaders');
        }
    }
})();
